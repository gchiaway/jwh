package com.seed.core.service.impl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.seed.core.exception.DAOException;
import com.seed.core.exception.ServiceException;
import com.seed.core.manager.Manager;
import com.seed.core.service.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 基于通用MyBatis manager插件的Service接口的实现
 *
 * @author gchiaway
 * 日期: 2020-01-17
 * 时间: 14:30
 */
public abstract class AbstractService<M extends Manager<T>, Ma extends BaseMapper<T>, T> extends ServiceImpl<Ma, T> implements Service<T> {

    @Autowired
    protected M baseManager;

    /**
     * 批量持久化
     *
     * @param models 需要保存的对象们
     * @return 是否成功
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public Boolean save(List<T> models) throws ServiceException {
        try {
            return this.baseManager.save(models);
        } catch (DAOException e) {
            throw new ServiceException(e, models);
        }
    }

    /**
     * 根据主键删除
     *
     * @param id 主键
     * @return 是否成功
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public Boolean deleteById(Object id) throws ServiceException {
        try {
            return this.baseManager.deleteById(id);
        } catch (DAOException e) {
            throw new ServiceException(e, id);
        }
    }

    /**
     * 批量刪除
     *
     * @param ids eg：ids -> “1,2,3,4”
     * @return 是否成功
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public Boolean deleteByIds(String ids) throws ServiceException {
        try {
            return this.baseManager.deleteByIds(ids);
        } catch (DAOException e) {
            throw new ServiceException(e, ids);
        }
    }


    /**
     * 批量更新
     *
     * @param newData   更新的数据
     * @param queryData 查询条件
     * @return 是否成功
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public Boolean updateBatch(T newData, T queryData) throws ServiceException {
        try {
            return this.baseManager.updateBatch(newData, queryData);
        } catch (DAOException e) {
            throw new ServiceException(e, newData, queryData);
        }
    }

    /**
     * 通过主键查找
     *
     * @param id 主键
     * @return 查找到的单个对象
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public T getModel(Object id) throws ServiceException {
        try {
            return this.baseManager.getModel(id);
        } catch (DAOException e) {
            throw new ServiceException(e, id);
        }
    }

    /**
     * 通过多个ID查找
     *
     * @param ids eg：ids -> “1,2,3,4”
     * @return 查到的多个对象
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public List<T> listModels(String ids) throws ServiceException {
        try {
            return this.baseManager.listModels(ids);
        } catch (DAOException e) {
            throw new ServiceException(e, ids);
        }
    }


    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）查找,value需符合unique约束
     *
     * @param fieldName 字段名称
     * @param value     字段值
     * @return 查找到的对象
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public List<T> listModels(String fieldName, Object value) throws ServiceException {
        try {
            return this.baseManager.listModels(fieldName, value);
        } catch (DAOException e) {
            throw new ServiceException(e, fieldName + ":" + value);
        }
    }

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）分页查找,value需符合unique约束
     *
     * @param fieldName 字段名称
     * @param value     字段值
     * @param page      分页条件
     * @return 查找到的对象
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public IPage<T> listModels(String fieldName, Object value, Page<T> page) throws ServiceException {
        try {
            return this.baseManager.listModels(fieldName, value, page);
        } catch (DAOException e) {
            throw new ServiceException(e, fieldName + ":" + value, page);
        }
    }

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）查找,value需符合unique约束
     *
     * @param model 对象
     * @return 查找到的对象列表
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public List<T> listModels(T model) throws ServiceException {
        try {
            return this.baseManager.listModels(model);
        } catch (DAOException e) {
            throw new ServiceException(e, model);
        }
    }

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）查找,value需符合unique约束
     *
     * @param model 对象
     * @param page  分页条件
     * @return 查找到的单个对象
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public IPage<T> listModels(T model, Page<T> page) throws ServiceException {
        try {
            return this.baseManager.listModels(model, page);
        } catch (DAOException e) {
            throw new ServiceException(e, model, page);
        }
    }


    /**
     * 获取所有
     *
     * @return 查到的多个对象
     * @throws ServiceException 业务层通用错误
     */
    @Override
    public List<T> listModels() throws ServiceException {
        try {
            return this.baseManager.listAllModels();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
