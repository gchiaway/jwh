package com.seed.core.vo;

import com.seed.core.vo.data.BaseData;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author gchiaway
 * 日期: 2020-04-16
 * 时间: 12:14
 */
@Getter
@Setter
@ToString
public class BaseModelVO<T extends BaseData> extends BaseVO {

    public BaseModelVO() {
        super();
    }

    public BaseModelVO(T data) {
        super();
        this.data = data;
    }

    /**
     * 数据
     */
    @ApiModelProperty(value = "数据")
    private T data;

}
