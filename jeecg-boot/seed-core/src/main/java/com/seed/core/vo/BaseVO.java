package com.seed.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author gchiaway
 * 日期: 2020-02-23
 * 时间: 0:26
 */
@Getter
@Setter
@ToString
public class BaseVO {
    /**
     * 是否成功
     */
    @ApiModelProperty(value = "是否成功", example = "true")
    private Boolean success;

    /**
     * 提示信息
     */
    @ApiModelProperty(value = "提示信息", example = "操作成功")
    private String msg;

    /**
     * 提示码
     */
    @ApiModelProperty(value = "提示码")
    private String msgCode;

    public BaseVO() {
        this.success = true;
        this.msg = "操作成功";
        this.msgCode = "";
    }
}
