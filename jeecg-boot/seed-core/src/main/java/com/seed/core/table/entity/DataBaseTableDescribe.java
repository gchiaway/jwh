package com.seed.core.table.entity;

import lombok.Data;

/**
 * 表描述
 *
 * @author gchiaway
 * 日期: 2020-02-12
 * 时间: 18:55
 */
@Data
public class DataBaseTableDescribe {

    private DataBaseTableDescribe() {
    }

    public DataBaseTableDescribe(String projectName, String tablePrefix) {
        //判断前缀末端是否添加了下划线
        String underLine = "_";
        if (!underLine.equals(projectName.substring(projectName.length() - 1, projectName.length()))) {
            //末尾没有有下划线
            StringBuilder sb = new StringBuilder(projectName);
            sb.insert(projectName.length(), "_");
            this.projectName = sb.toString();
        } else {
            this.projectName = projectName;
        }
        if (!underLine.equals(tablePrefix.substring(tablePrefix.length() - 1, tablePrefix.length()))) {
            //末尾没有有下划线
            StringBuilder sb = new StringBuilder(tablePrefix);
            sb.insert(tablePrefix.length(), "_");
            this.tablePrefix = sb.toString();
        } else {
            this.tablePrefix = tablePrefix;
        }
    }

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 表前缀
     */
    private String tablePrefix;
}
