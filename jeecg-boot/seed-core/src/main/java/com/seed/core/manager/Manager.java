package com.seed.core.manager;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.seed.core.exception.DAOException;

import java.util.List;

/**
 * 持久层对象
 *
 * @author gchiaway
 * 日期: 2020-01-17
 * 时间: 14:29
 */
public interface Manager<T> {
    /**
     * 持久化
     *
     * @param model 需要保存的对象
     * @return 是否成功
     * @throws DAOException 持久层通用错误
     */
    Boolean save(T model) throws DAOException;

    /**
     * 批量持久化
     *
     * @param models 需要保存的对象们
     * @return 是否成功
     * @throws DAOException 持久层通用错误
     */
    Boolean save(List<T> models) throws DAOException;

    /**
     * 根据主键删除
     *
     * @param id 主键
     * @return 是否成功
     * @throws DAOException 持久层通用错误
     */
    Boolean deleteById(Object id) throws DAOException;

    /**
     * 批量刪除
     *
     * @param ids eg：ids -> “1,2,3,4”
     * @return 是否成功
     * @throws DAOException 持久层通用错误
     */
    Boolean deleteByIds(String ids) throws DAOException;

    /**
     * 更新，完全更新，更新所有字段，包括为空的字段
     *
     * @param model 需要更新的对象
     * @return 是否成功
     * @throws DAOException 持久层通用错误
     */
    Boolean updateById(T model) throws DAOException;

    /**
     * 批量更新
     *
     * @param newData   更新的数据
     * @param queryData 查询条件
     * @return 是否成功
     * @throws DAOException 持久层通用错误
     */
    Boolean updateBatch(T newData, T queryData) throws DAOException;

    /**
     * 通过主键查找
     *
     * @param id 主键
     * @return 查找到的单个对象
     * @throws DAOException 持久层通用错误
     */
    T getModel(Object id) throws DAOException;

    /**
     * 通过多个ID查找
     *
     * @param ids eg：ids -> “1,2,3,4”
     * @return 查到的多个对象
     * @throws DAOException 持久层通用错误
     */
    List<T> listModels(String ids) throws DAOException;

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）查找,value需符合unique约束
     *
     * @param fieldName 字段名称
     * @param value     字段值
     * @return 查找到的对象
     * @throws DAOException 持久层通用错误
     */
    List<T> listModels(String fieldName, Object value) throws DAOException;

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）分页查找,value需符合unique约束
     *
     * @param fieldName 字段名称
     * @param value     字段值
     * @param page      分页条件
     * @return 查找到的对象
     * @throws DAOException 持久层通用错误
     */
    IPage<T> listModels(String fieldName, Object value, Page<T> page) throws DAOException;

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）查找,value需符合unique约束
     *
     * @param model 对象
     * @return 查找到的单个对象
     * @throws DAOException 持久层通用错误
     */
    List<T> listModels(T model) throws DAOException;

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）查找,value需符合unique约束
     *
     * @param model 对象
     * @param page  分页条件
     * @return 查找到的单个对象
     * @throws DAOException 持久层通用错误
     */
    IPage<T> listModels(T model, Page<T> page) throws DAOException;

    /**
     * 获取所有
     *
     * @return 查到的多个对象
     * @throws DAOException 持久层通用错误
     */
    List<T> listAllModels() throws DAOException;
}
