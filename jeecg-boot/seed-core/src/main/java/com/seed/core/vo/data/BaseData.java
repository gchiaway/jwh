package com.seed.core.vo.data;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author gchiaway
 * 日期: 2020-04-16
 * 时间: 12:08
 */
@Getter
@Setter
@ToString
public class BaseData {

}
