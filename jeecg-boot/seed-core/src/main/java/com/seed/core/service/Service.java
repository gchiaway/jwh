package com.seed.core.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.seed.core.exception.ServiceException;

import java.util.List;

/**
 * Service 层 基础接口，其他Service 接口 请继承该接口
 *
 * @author gchiaway
 * 日期: 2020-01-17
 * 时间: 14:30
 */
public interface Service<T> extends IService<T> {

    /**
     * 批量持久化
     *
     * @param models 需要保存的对象们
     * @return 是否成功
     * @throws ServiceException 业务层通用错误
     */
    Boolean save(List<T> models) throws ServiceException;

    /**
     * 根据主键删除
     *
     * @param id 主键
     * @return 是否成功
     * @throws ServiceException 业务层通用错误
     */
    Boolean deleteById(Object id) throws ServiceException;

    /**
     * 批量刪除
     *
     * @param ids eg：ids -> “1,2,3,4”
     * @return 是否成功
     * @throws ServiceException 业务层通用错误
     */
    Boolean deleteByIds(String ids) throws ServiceException;

    /**
     * 批量更新
     *
     * @param newData   更新的数据
     * @param queryData 查询条件
     * @return 是否成功
     * @throws ServiceException 业务层通用错误
     */
    Boolean updateBatch(T newData, T queryData) throws ServiceException;

    /**
     * 通过主键查找
     *
     * @param id 主键
     * @return 查找到的单个对象
     * @throws ServiceException 业务层通用错误
     */
    T getModel(Object id) throws ServiceException;

    /**
     * 通过多个ID查找
     *
     * @param ids eg：ids -> “1,2,3,4”
     * @return 查到的多个对象
     * @throws ServiceException 业务层通用错误
     */
    List<T> listModels(String ids) throws ServiceException;

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）查找,value需符合unique约束
     *
     * @param fieldName 字段名称
     * @param value     字段值
     * @return 查找到的对象
     * @throws ServiceException 业务层通用错误
     */
    List<T> listModels(String fieldName, Object value) throws ServiceException;

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）分页查找,value需符合unique约束
     *
     * @param fieldName 字段名称
     * @param value     字段值
     * @param page      分页条件
     * @return 查找到的对象
     * @throws ServiceException 业务层通用错误
     */
    IPage<T> listModels(String fieldName, Object value, Page<T> page) throws ServiceException;

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）查找,value需符合unique约束
     *
     * @param model 对象
     * @return 查找到的单个对象
     * @throws ServiceException 业务层通用错误
     */
    List<T> listModels(T model) throws ServiceException;

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）查找,value需符合unique约束
     *
     * @param model 对象
     * @param page  分页条件
     * @return 查找到的单个对象
     * @throws ServiceException 业务层通用错误
     */
    IPage<T> listModels(T model, Page<T> page) throws ServiceException;

    /**
     * 获取所有
     *
     * @return 查到的多个对象
     * @throws ServiceException 业务层通用错误
     */
    List<T> listModels() throws ServiceException;

}
