package com.seed.core.query;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author gchiaway
 * 日期: 2020-02-21
 * 时间: 15:42
 */
@Getter
@Setter
@ToString
public class BaseQuery {
    /**
     * 页数
     */
    @ApiModelProperty(value = "页数", example = "1")
    private Integer queryPage;

    /**
     * 每页条数
     */
    @ApiModelProperty(value = "每页条数", example = "5")
    private Integer querySize;
}
