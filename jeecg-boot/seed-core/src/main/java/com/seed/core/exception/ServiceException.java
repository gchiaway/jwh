package com.seed.core.exception;

/**
 * 业务层通用错误
 *
 * @author gchiaway
 * 日期: 2020-01-17
 * 时间: 15:18
 */
public class ServiceException extends BaseException {

    private static final long serialVersionUID = 3350597437992576425L;

    /**
     * 无参数构造方法
     *
     * @param e 异常对象
     */
    public ServiceException(Exception e) {
        super(e);
    }

    /**
     * 有参数构造方法
     *
     * @param e          异常对象
     * @param parameters 请求参数
     */
    public ServiceException(Exception e, Object... parameters) {
        super(e, parameters);
    }

    /**
     * 无参数构造方法
     *
     * @param e 持久层异常对象
     */
    public ServiceException(DAOException e) {
        super(e.getException(), e.getParameters());
    }

}
