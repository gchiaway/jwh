package com.seed.core.table.entity;

import lombok.Data;

/**
 * 数据库对象转换为java名称
 * 数据库名称由三部分组成
 * 项目名称_表前缀_实体名称
 *
 * @author gchiaway
 * 日期: 2019-09-10
 * 时间: 19:38
 */
@Data
public class DataBaseTable {
    /**
     * 项目名称
     * eg: seed_
     */
    private String projectName;

    /**
     * 表前缀
     * eg: t_
     */
    private String tablePrefix;

    /**
     * 表注释
     * eg: 测试表
     */
    private String tableComment;

    /**
     * 表名
     * eg: seed_t_test_table
     */
    private String tableName;

    /**
     * 带前缀的小写名称
     * eg: seedttesttable
     */
    private String tableNameWithOutUnderline;

    /**
     * 实体名称
     * eg: test_table
     */
    private String entityName;

    /**
     * 实体名称没有下划线
     * eg: testtable
     */
    private String entityNameWithOutUnderline;

    /**
     * 实体驼峰名称
     * eg: TestTable
     */
    private String entityNameHump;

    /**
     * 实体驼峰名称首字母小写
     * eg: testTable
     */
    private String entityNameHumpFirstLow;
}
