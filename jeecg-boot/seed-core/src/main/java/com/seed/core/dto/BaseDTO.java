package com.seed.core.dto;

import lombok.Data;

/**
 * @author gchiaway
 * 日期: 2020-03-19
 * 时间: 15:24
 */
@Data
public class BaseDTO {
    /**
     * 是否成功
     */
    private Boolean success;

    /**
     * 提示信息
     */
    private String msg;

    public BaseDTO() {
        this.success = true;
        this.msg = "操作成功";
    }
}
