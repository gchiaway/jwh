package com.seed.core.exception;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 基础异常类
 *
 * @author gchiaway
 * 日期: 2020-01-17
 * 时间: 15:03
 */
public abstract class BaseException extends Exception {

    private static final long serialVersionUID = 822075583116729965L;
    /**
     * 异常对象
     */
    private Exception exception;
    /**
     * 异常信息
     */
    private String message;
    /**
     * 请求参数
     */
    private List<Object> parameters = new ArrayList<>();

    /**
     * 无参数构造方法
     *
     * @param e 异常对象
     */
    public BaseException(Exception e) {
        this.exception = e;
        this.message = e.getMessage();
    }

    /**
     * 有参数构造方法
     *
     * @param e          异常对象
     * @param parameters 请求参数
     */
    public BaseException(Exception e, Object... parameters) {
        this.exception = e;
        this.message = e.getMessage();
        this.parameters.addAll(Arrays.asList(parameters));
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Object> getParameters() {
        return parameters;
    }

    public void setParameters(List<Object> parameters) {
        this.parameters = parameters;
    }
}
