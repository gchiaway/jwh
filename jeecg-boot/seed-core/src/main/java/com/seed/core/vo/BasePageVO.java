package com.seed.core.vo;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.seed.core.vo.data.BaseData;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * @author gchiaway
 * 日期: 2020-02-21
 * 时间: 15:51
 */
@Getter
@Setter
@ToString
public class BasePageVO<T extends BaseData> extends BaseVO {

    public BasePageVO() {

    }

    public BasePageVO(IPage page, List<T> list) {
        this.page = ((Long) page.getCurrent()).intValue();
        this.size = ((Long) page.getSize()).intValue();
        this.totalPage = ((Long) page.getPages()).intValue();
        this.totalSize = ((Long) page.getTotal()).intValue();
        this.list = list;
    }

    @ApiModelProperty(value = "页数", example = "1")
    private Integer page;

    @ApiModelProperty(value = "每页条数", example = "5")
    private Integer size;

    @ApiModelProperty(value = "总页数", example = "1")
    private Integer totalPage;

    @ApiModelProperty(value = "总条数", example = "5")
    private Integer totalSize;

    @ApiModelProperty(value = "数据")
    private List<T> list;
}
