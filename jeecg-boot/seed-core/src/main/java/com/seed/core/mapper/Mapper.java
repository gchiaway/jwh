package com.seed.core.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * mapper基础类
 *
 * @param <T> 任意实体类型
 * @author gchiaway
 */
public interface Mapper<T> extends BaseMapper<T> {

}
