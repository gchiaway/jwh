package com.seed.core.exception;

/**
 * 持久层错误
 *
 * @author gchiaway
 * 日期: 2020-01-26
 * 时间: 17:06
 */
public class DAOException extends BaseException {
    private static final long serialVersionUID = -1838101064214830799L;

    /**
     * 无参数构造方法
     *
     * @param e 异常对象
     */
    public DAOException(Exception e) {
        super(e);
    }

    /**
     * 有参数构造方法
     *
     * @param e          异常对象
     * @param parameters 请求参数
     */
    public DAOException(Exception e, Object... parameters) {
        super(e, parameters);
    }
}
