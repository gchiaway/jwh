package com.seed.core.table;

import com.seed.core.table.entity.DataBaseTable;
import com.seed.core.table.entity.DataBaseTableDescribe;

import java.util.List;

/**
 * @author gchiaway
 * 日期: 2020-02-12
 * 时间: 17:58
 */
public interface DataBaseTableServiceI {

    /**
     * 查看数据库名称
     *
     * @return 数据库名称
     */
    String getDataBaseName();

    /**
     * 设置数据库名称
     *
     * @param dataBaseName 数据库名称
     */
    void setDataBaseName(String dataBaseName);

    /**
     * 根据数据库前缀查找符合条件的表
     *
     * @param dataBaseTableDescribe 数据表名称前缀
     * @param tableNames            表名
     * @return 查出的表
     */
    List<DataBaseTable> listDataBaseTablesByTableNamesPrefix(DataBaseTableDescribe dataBaseTableDescribe, String... tableNames);
}
