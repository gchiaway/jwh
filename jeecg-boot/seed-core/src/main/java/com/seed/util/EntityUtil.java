package com.seed.util;

import org.springframework.cglib.beans.BeanMap;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * 针对实体进行操作判断的工具类
 *
 * @author gchiaway
 * 日期: 2019-04-18
 * 时间: 16:51
 */
public class EntityUtil {
    /**
     * 传入任意实体转换为map对象，然后可以用get(属性名)的方法获取值，不会产生空指针异常
     *
     * @param bean 任意实体
     * @param <T>  任意实体类型
     * @return 返回k-v形式的 属性-值 的map对象
     */
    public static <T> Map<String, Object> beanToMap(T bean) {
        Map<String, Object> map = new HashMap<>(16);
        if (bean != null) {
            BeanMap beanMap = BeanMap.create(bean);
            for (Object key : beanMap.keySet()) {
                map.put(key + "", beanMap.get(key));
            }
        }
        return map;
    }

    /**
     * 判断对象是否为空
     *
     * @param object 任意对象
     * @return 返回true(为空)/false(不为空)
     * @throws Exception 抛出异常
     */
    public static boolean isObjectEmpty(Object object) throws Exception {
        if (StringUtils.isEmpty(object)) {
            return true;
        }

        if (EntityUtil.isAnyFieldNull(object)) {
            return true;
        }

        return false;
    }


    /**
     * 判断该对象是否全部属性为空
     *
     * @param object 目标对象
     * @return 返回true表示有属性为null  返回false表示所有属性都不是null
     * @throws Exception 捕获异常
     */
    public static boolean isAnyFieldNull(Object object) throws Exception {
        Field[] fs = EntityUtil.getFields(object);
        boolean flag = false;
        //遍历属性
        for (Field f : fs) {
            // 设置属性是可以访问的(私有的也可以)
            f.setAccessible(true);
            // 得到此属性的值
            Object val = f.get(object);
            //只要有1个属性为空,那么就是并非所有的属性值都为不为空
            if (val == null || "".equals(val)) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    /**
     * 判断该对象是否全部属性为空
     *
     * @param object 目标对象
     * @return 返回true表示所有属性为null  返回false表示不是所有属性都是null
     * @throws Exception
     */
    public static boolean isAllFieldNull(Object object) throws Exception {
        Field[] fs = EntityUtil.getFields(object);
        boolean flag = true;
        //遍历属性
        for (Field f : fs) {
            // 设置属性是可以访问的(私有的也可以)
            f.setAccessible(true);
            // 得到此属性的值
            Object val = f.get(object);
            //只要有1个属性不为空,那么就不是所有的属性值都为空
            if (val != null) {
                flag = false;
                break;
            }
        }
        return flag;
    }

    /**
     * 传入任意对象，返回全部的字段
     *
     * @param object 任意对象
     * @return 字段数组
     * @throws Exception 抛出任意异常
     */
    private static Field[] getFields(Object object) throws Exception {
        // 得到类对象
        Class objectClass = object.getClass();
        //得到属性集合
        return objectClass.getDeclaredFields();
    }

}
