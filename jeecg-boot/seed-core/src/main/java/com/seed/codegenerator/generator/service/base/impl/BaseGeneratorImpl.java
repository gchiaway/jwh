package com.seed.codegenerator.generator.service.base.impl;

import com.seed.codegenerator.generator.config.GenConfig;
import com.seed.codegenerator.generator.entity.FtlConfig;
import com.seed.codegenerator.generator.entity.PageData;
import com.seed.codegenerator.generator.service.base.BaseGeneratorI;
import com.seed.util.EntityUtil;
import freemarker.template.Configuration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileWriter;
import java.util.List;

/**
 * @author gchiaway
 * 日期: 2020-02-14
 * 时间: 17:23
 */
@Slf4j
@Service("baseGeneratorService")
@Transactional(rollbackFor = Exception.class)
public class BaseGeneratorImpl implements BaseGeneratorI {

    /**
     * 生成配置文件
     */
    @Autowired
    private GenConfig genConfig;


    /**
     * 根据模板生成文件
     *
     * @param dirPath       生成目标路径
     * @param configuration 模板配置文件
     * @param pageData      页面数据
     * @param ftlConfig     模板配置文件
     * @return 是否成功
     */
    private Boolean genFile(String dirPath,
                            Configuration configuration,
                            PageData pageData,
                            FtlConfig ftlConfig) {
        String fileName = File.separator + pageData.getEntityNameHump() + ftlConfig.getTargetNameSuffix();
        File tempFile = new File(dirPath + fileName);
        if (!tempFile.getParentFile().exists()) {
            if (!tempFile.getParentFile().mkdirs()) {
                return false;
            }
        }
        try {
            configuration.getTemplate(ftlConfig.getFtlFullPath())
                    .process(EntityUtil.beanToMap(pageData), new FileWriter(tempFile));
        } catch (Exception e) {
            log.error("模板生成文件失败");
            return false;
        }
        return true;
    }

    /**
     * 根据模板生成文件
     *
     * @param dirPath       生成目标路径
     * @param configuration 模板配置文件
     * @param pageData      页面数据
     * @param ftlConfigList 模板配置文件
     * @return 是否成功
     */
    @Override
    public Boolean genFile(String dirPath, Configuration configuration, PageData pageData, List<FtlConfig> ftlConfigList) {
        if (!dirPath.endsWith(File.separator)) {
            dirPath = dirPath + File.separator;
        }
        dirPath = dirPath + genConfig.getCodeFileTargetDirectory();
        for (FtlConfig ftlConfig : ftlConfigList) {
            String targetDir = dirPath + "/" + pageData.getModule() + ftlConfig.getTargetDirPath();
            Boolean isThisSuccess = this.genFile(targetDir, configuration, pageData, ftlConfig);
            if (!isThisSuccess) {
                return false;
            }
        }
        return true;
    }
}
