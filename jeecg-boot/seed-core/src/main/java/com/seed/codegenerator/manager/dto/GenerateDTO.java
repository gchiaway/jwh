package com.seed.codegenerator.manager.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 代码生成的dto
 *
 * @author gchiaway
 * 日期: 2020-04-26
 * 时间: 15:35
 */
@Getter
@Setter
@ToString
public class GenerateDTO {
    public GenerateDTO() {
        this.setSuccess(true);
        this.setMsg("生成成功");
    }

    /**
     * 是否成功
     */
    private Boolean success;
    /**
     * 信息
     */
    private String msg;
    /**
     * 信息编码
     */
    private String code;
}
