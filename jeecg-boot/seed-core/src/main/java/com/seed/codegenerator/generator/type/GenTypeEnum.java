package com.seed.codegenerator.generator.type;

/**
 * 生成策略枚举
 *
 * @author gchiaway
 * 日期: 2019-11-06
 * 时间: 20:15
 */
public enum GenTypeEnum {
    /**
     * 生成通用型模板
     */
    UNIVERSAL("universal", "universal"),
    ;
    /**
     * 策略名称
     */
    private String type;
    /**
     * 模板位置
     */
    private String path;

    GenTypeEnum(String type, String path) {
        this.type = type;
        this.path = path;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    /**
     * 获取模板位置
     *
     * @return 拼接完成的模板位置
     */
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
