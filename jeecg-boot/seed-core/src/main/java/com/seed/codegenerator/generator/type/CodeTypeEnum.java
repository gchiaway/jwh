package com.seed.codegenerator.generator.type;

/**
 * 代码类型
 *
 * @author gchiaway
 * 日期: 2020-05-04
 * 时间: 13:46
 */
public enum CodeTypeEnum {
    /**
     * 单表
     */
    SINGLE("单表"),
    /**
     * 一对多
     */
    ONE_TO_MANY("一对多"),
    ;
    /**
     * 代码类型
     */
    private String type;

    CodeTypeEnum(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
