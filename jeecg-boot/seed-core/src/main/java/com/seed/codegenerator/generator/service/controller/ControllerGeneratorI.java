package com.seed.codegenerator.generator.service.controller;

import com.seed.codegenerator.generator.entity.FtlConfig;
import com.seed.codegenerator.generator.entity.PageData;
import com.seed.codegenerator.generator.service.base.BaseGeneratorI;
import freemarker.template.Configuration;

import java.util.List;

/**
 * @author gchiaway
 * 日期: 2020-02-12
 * 时间: 18:26
 */
public interface ControllerGeneratorI extends BaseGeneratorI {

    /**
     * 生成相关的控制层
     *
     * @param dirPath       生成目标路径
     * @param configuration ftl信息
     * @param pageData      页面数据
     * @param ftlConfigList 配置文件信息列表
     * @return 是否成功
     */
    Boolean genController(String dirPath, Configuration configuration, PageData pageData, List<FtlConfig> ftlConfigList);

}
