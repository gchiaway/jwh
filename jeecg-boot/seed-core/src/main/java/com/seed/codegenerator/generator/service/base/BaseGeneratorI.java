package com.seed.codegenerator.generator.service.base;

import com.seed.codegenerator.generator.entity.FtlConfig;
import com.seed.codegenerator.generator.entity.PageData;
import freemarker.template.Configuration;

import java.util.List;

/**
 * @author gchiaway
 * 日期: 2020-02-14
 * 时间: 17:21
 */
public interface BaseGeneratorI {
    /**
     * 根据模板生成文件
     *
     * @param dirPath       生成目标路径
     * @param configuration 模板配置文件
     * @param pageData      页面数据
     * @param ftlConfigList 模板配置文件
     * @return 是否成功
     */
    Boolean genFile(String dirPath, Configuration configuration, PageData pageData, List<FtlConfig> ftlConfigList);
}
