package com.seed.codegenerator.generator.entity;

import lombok.Data;

import java.util.List;

/**
 * @author gchiaway
 * 日期: 2020-05-04
 * 时间: 13:50
 */
@Data
public class FtlList {

    /**
     * controller模板文件
     */
    private List<FtlConfig> controllerFtlList;

    /**
     * service模板文件
     */
    private List<FtlConfig> serviceFtlList;

    /**
     * manager模板文件
     */
    private List<FtlConfig> managerFtlList;

    /**
     * model模板文件
     */
    private List<FtlConfig> modelFtlList;
}
