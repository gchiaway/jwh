package com.seed.codegenerator.generator.entity;

import lombok.Data;

/**
 * @author gchiaway
 * 日期: 2020-03-23
 * 时间: 17:05
 */
@Data
public class TemplateConfig {

    /**
     * 模板名称
     */
    private String templateName;

    /**
     * 版本
     */
    private String version;

    /**
     * 单表
     */
    private FtlList single;

    /**
     * 一对多
     */
    private FtlList oneToMany;
}
