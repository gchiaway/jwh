package com.seed.codegenerator.generator.entity;

import com.seed.codegenerator.generator.config.GenConfig;
import com.seed.codegenerator.generator.type.GenTypeEnum;
import com.seed.core.table.entity.DataBaseTable;
import lombok.Data;
import org.springframework.beans.BeanUtils;

/**
 * 生成代码时页面使用的值
 *
 * @author gchiaway
 * 日期: 2020-02-12
 * 时间: 22:53
 */
@Data
public class PageData {

    /**
     * 构造用于生成代码的页面对象
     *
     * @param dataBaseTable 表信息
     * @param genConfig     配置信息
     */
    public PageData(DataBaseTable dataBaseTable,
                    GenConfig genConfig,
                    GenTypeEnum genTypeEnum) {
        BeanUtils.copyProperties(dataBaseTable, this);
        this.projectName = this.projectName.replace("_", "");
        this.tablePrefix = this.tablePrefix.replace("_", "");
        this.author = genConfig.getAuthor();
        this.date = genConfig.getDate();
        this.genTypeEnum = genTypeEnum;
        this.basePackage = genConfig.getApiBasePackage() + "." + this.getProjectName();
        this.bussPackage = genConfig.getBussBasePackage() + "." + this.getProjectName();
    }

    /**
     * 获取模块名称
     * 项目名称+分割符+实体名称
     *
     * @return 模块名称
     */
    public String getModule() {
        return this.getProjectName() + "/" + this.getEntityNameWithOutUnderline();
    }


    /**
     * 项目名称
     * eg: seed_
     */
    private String projectName;

    /**
     * 表前缀
     * eg: t_
     */
    private String tablePrefix;

    /**
     * 表注释
     * eg: 测试表
     */
    private String tableComment;

    /**
     * 表名
     * eg: seed_t_test_table
     */
    private String tableName;

    /**
     * 带前缀的小写名称
     * eg: seedttesttable
     */
    private String tableNameWithOutUnderline;

    /**
     * 实体名称
     * eg: test_table
     */
    private String entityName;

    /**
     * 实体名称没有下划线
     * eg: testtable
     */
    private String entityNameWithOutUnderline;

    /**
     * 实体驼峰名称
     * eg: TestTable
     */
    private String entityNameHump;

    /**
     * 实体驼峰名称首字母小写
     * eg: testTable
     */
    private String entityNameHumpFirstLow;

    /**
     * 作者信息
     */
    private String author;

    /**
     * 日期信息
     */
    private String date;

    /**
     * 根目录
     */
    private String basePackage;
    /**
     * 根目录
     */
    private String bussPackage;

    /**
     * 生成策略
     */
    private GenTypeEnum genTypeEnum;
}
