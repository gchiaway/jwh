package com.seed.codegenerator.manager;

import com.seed.codegenerator.generator.type.CodeTypeEnum;
import com.seed.codegenerator.generator.type.GenTypeEnum;
import com.seed.codegenerator.manager.dto.GenerateDTO;
import com.seed.core.table.entity.DataBaseTableDescribe;

/**
 * @author gchiaway
 * 日期: 2020-02-12
 * 时间: 21:30
 */
public interface CodeGeneratorI {

    /**
     * 根据类型生成代码
     *
     * @param outFilePath           输出目录
     * @param genTypeEnum           代码生成策略
     * @param codeTypeEnum          代码生成类型
     * @param dataBaseTableDescribe 表前缀
     * @param tableNames            表名
     * @return 生成结果
     */
    GenerateDTO generateCode(String outFilePath, GenTypeEnum genTypeEnum, CodeTypeEnum codeTypeEnum, DataBaseTableDescribe dataBaseTableDescribe, String... tableNames);

    /**
     * 生成通用代码
     *
     * @param outFilePath           输出目录
     * @param codeTypeEnum          代码生成类型
     * @param dataBaseTableDescribe 表前缀
     * @param tableNames            表名
     * @return 生成结果
     */
    GenerateDTO generateUniversalCode(String outFilePath, CodeTypeEnum codeTypeEnum, DataBaseTableDescribe dataBaseTableDescribe, String... tableNames);

}
