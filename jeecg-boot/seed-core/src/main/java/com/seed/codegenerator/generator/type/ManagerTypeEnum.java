package com.seed.codegenerator.generator.type;

/**
 * manager 生成策略
 *
 * @author gchiaway
 * 日期: 2020-04-09
 * 时间: 22:04
 */
public enum ManagerTypeEnum {
    /**
     * mybatis plus
     */
    MYBATIS_PLUS("mybatis plus", "AbstractManager"),
    ;
    /**
     * 策略名称
     */
    private String type;
    /**
     * 策略实现类
     */
    private String className;

    ManagerTypeEnum(String type, String className) {
        this.type = type;
        this.className = className;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
