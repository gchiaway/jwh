package com.seed.codegenerator.generator.service.controller.impl;

import com.seed.codegenerator.generator.entity.FtlConfig;
import com.seed.codegenerator.generator.entity.PageData;
import com.seed.codegenerator.generator.service.base.impl.BaseGeneratorImpl;
import com.seed.codegenerator.generator.service.controller.ControllerGeneratorI;
import freemarker.template.Configuration;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author gchiaway
 * 日期: 2020-02-12
 * 时间: 21:52
 */
@Service("controllerGeneratorService")
@Transactional(rollbackFor = Exception.class)
public class ControllerGeneratorImpl extends BaseGeneratorImpl implements ControllerGeneratorI {
    /**
     * 生成相关的控制层
     *
     * @param dirPath       生成目标路径
     * @param configuration ftl信息
     * @param pageData      页面数据
     * @param ftlConfigList 配置文件信息列表
     * @return 是否成功
     */
    @Override
    public Boolean genController(String dirPath, Configuration configuration, PageData pageData, List<FtlConfig> ftlConfigList) {
        return this.genFile(dirPath, configuration, pageData, ftlConfigList);
    }
}
