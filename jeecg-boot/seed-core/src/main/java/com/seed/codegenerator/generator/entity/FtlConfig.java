package com.seed.codegenerator.generator.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.File;

/**
 * @author gchiaway
 * 日期: 2020-03-23
 * 时间: 17:08
 */
@Getter
@Setter
@ToString
public class FtlConfig {

    /**
     * 目录路径
     */
    private String ftlDirPath;

    /**
     * 文件名称
     */
    private String ftlName;

    /**
     * 目录路径
     */
    private String targetDirPath;

    /**
     * 生成文件名称后缀
     */
    private String targetNameSuffix;

    /**
     * 获取模板完整路径
     *
     * @return 拼接过的模板路径
     */
    public String getFtlFullPath() {
        return this.getFtlDirPath() + File.separator + this.getFtlName();
    }

}
