package com.seed.codegenerator.generator.service.manager;

import com.seed.codegenerator.generator.entity.FtlConfig;
import com.seed.codegenerator.generator.entity.PageData;
import com.seed.codegenerator.generator.service.base.BaseGeneratorI;
import freemarker.template.Configuration;

import java.util.List;

/**
 * @author gchiaway
 * 日期: 2020-03-23
 * 时间: 19:45
 */
public interface ManagerGeneratorI extends BaseGeneratorI {
    /**
     * 生成相关的通用业务层
     *
     * @param dirPath       生成目标路径
     * @param configuration ftl信息
     * @param pageData      页面数据
     * @param ftlConfigList 配置文件信息列表
     * @return 是否成功
     */
    Boolean genManager(String dirPath, Configuration configuration, PageData pageData, List<FtlConfig> ftlConfigList);

}
