package com.seed.codegenerator.generator.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author gchiaway
 * 日期: 2020-02-12
 * 时间: 22:21
 */
@Getter
@Component
public class GenConfig {

    public GenConfig() {
        javaPath = "src/main/java";
        resourcesPath = "src/main/resources";
        underResourcesTemplateFileDirectory = "generator/template/";
        mapperPath = resourcesPath + "/mapper";
    }

    /**
     * 业务包路径
     */
    @Value(value = "${code-generator.apppackage}")
    private String apiBasePackage;

    /**
     * 后台路径
     */
    @Value(value = "${code-generator.bussipackage}")
    private String bussBasePackage;

    /**
     * 模板位置
     */
    private String underResourcesTemplateFileDirectory;

    /**
     * java文件路径
     */
    private String javaPath;

    /**
     * 资源文件路径
     */
    private String resourcesPath;

    /**
     * mapper文件路径
     */
    private String mapperPath;

    /**
     * 作者信息
     */
    private final String author = "CodeGenerator";

    /**
     * 日期信息
     */
    private final String date = new SimpleDateFormat("yyyy年MM月dd日").format(new Date());

    /**
     * 获取生成代码位置的根级目录
     *
     * @return 路径
     */
    public String getCodeFileTargetDirectory() {
        return javaPath + this.packageConvertPath(apiBasePackage);
    }

    /**
     * 转换包路径
     *
     * @param packageName 包名称
     * @return 路径
     */
    private String packageConvertPath(String packageName) {
        return String.format("/%s/", packageName.contains(".") ? packageName.replaceAll("\\.", "/") : packageName);
    }

}
