package ${basePackage}.${entityNameWithOutUnderline}.dto;

import ${basePackage}.${entityNameWithOutUnderline}.entity.*;
import com.seed.core.dto.BaseDTO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
* ${tableComment}的DTO对象
* @author ${author}
* @date ${date}
*/
@Data
@EqualsAndHashCode(callSuper = true)
public class ${entityNameHump}DTO extends BaseDTO {

public ${entityNameHump}DTO() {

}

public ${entityNameHump}DTO(${entityNameHump} ${entityNameHumpFirstLow}) {

}

}