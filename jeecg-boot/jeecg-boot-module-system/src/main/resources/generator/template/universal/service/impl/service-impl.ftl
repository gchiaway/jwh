package ${basePackage}.${entityNameWithOutUnderline}.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import ${basePackage}.${entityNameWithOutUnderline}.from.${entityNameHump}From;
import ${basePackage}.${entityNameWithOutUnderline}.manager.${entityNameHump}ManagerI;
import ${basePackage}.${entityNameWithOutUnderline}.query.${entityNameHump}Query;
import ${basePackage}.${entityNameWithOutUnderline}.service.${entityNameHump}ServiceI;
import ${basePackage}.${entityNameWithOutUnderline}.vo.${entityNameHump}BriefPageVO;
import ${basePackage}.${entityNameWithOutUnderline}.vo.${entityNameHump}DetailPageVO;
import ${basePackage}.${entityNameWithOutUnderline}.vo.data.${entityNameHump}BriefVO;
import ${basePackage}.${entityNameWithOutUnderline}.vo.data.${entityNameHump}DetailVO;
import ${bussPackage}.${entityNameWithOutUnderline}.entity.${entityNameHump};
import ${bussPackage}.${entityNameWithOutUnderline}.mapper.${entityNameHump}Mapper;
import com.seed.core.exception.DAOException;
import com.seed.core.exception.ServiceException;
import com.seed.core.service.impl.AbstractService;
import com.seed.util.MyBeanUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;


/**
* @author ${author}
* @date ${date}
*/
@Slf4j
@Service("${entityNameHumpFirstLow}ApiService")
@Transactional(rollbackFor = ServiceException.class)
public class ${entityNameHump}ServiceImpl extends AbstractService
<${entityNameHump}ManagerI ,${entityNameHump}Mapper, ${entityNameHump}> implements ${entityNameHump}ServiceI {

    /**
    * 转换为VO对象
    *
    * @param ${entityNameHumpFirstLow} model
    * @return VO对象
    */
    private ${entityNameHump}DetailVO getDetailVO(${entityNameHump} ${entityNameHumpFirstLow}) {
    ${entityNameHump}DetailVO ${entityNameHumpFirstLow}DetailVO = new ${entityNameHump}DetailVO();
    BeanUtils.copyProperties(${entityNameHumpFirstLow}, ${entityNameHumpFirstLow}DetailVO);
    return ${entityNameHumpFirstLow}DetailVO;
    }

    /**
    * 转换为VO对象
    *
    * @param ${entityNameHumpFirstLow} model
    * @return VO对象
    */
    private ${entityNameHump}BriefVO getBriefVO(${entityNameHump} ${entityNameHumpFirstLow}) {
    ${entityNameHump}BriefVO ${entityNameHumpFirstLow}BriefVO = new ${entityNameHump}BriefVO();
    BeanUtils.copyProperties(${entityNameHumpFirstLow}, ${entityNameHumpFirstLow}BriefVO);
    return ${entityNameHumpFirstLow}BriefVO;
    }

    /**
    * 转换为VO对象
    *
    * @param ${entityNameHumpFirstLow}List model列表
    * @return VO对象列表
    */
    private List
    <${entityNameHump}DetailVO> listDetailVOS(List<${entityNameHump}> ${entityNameHumpFirstLow}List) {
        List
        <${entityNameHump}DetailVO> ${entityNameHumpFirstLow}DetailVOList = new ArrayList<>();
            for (${entityNameHump} ${entityNameHumpFirstLow} : ${entityNameHumpFirstLow}List) {
            ${entityNameHumpFirstLow}DetailVOList.add(this.getDetailVO(${entityNameHumpFirstLow}));
            }
            return ${entityNameHumpFirstLow}DetailVOList;
            }

            /**
            * 转换为VO对象
            *
            * @param ${entityNameHumpFirstLow}List model列表
            * @return VO对象列表
            */
            private List
            <${entityNameHump}BriefVO> listBriefVOS(List<${entityNameHump}> ${entityNameHumpFirstLow}List) {
                List
                <${entityNameHump}BriefVO> ${entityNameHumpFirstLow}BriefVOList = new ArrayList<>();
                    for (${entityNameHump} ${entityNameHumpFirstLow} : ${entityNameHumpFirstLow}List) {
                    ${entityNameHumpFirstLow}BriefVOList.add(this.getBriefVO(${entityNameHumpFirstLow}));
                    }
                    return ${entityNameHumpFirstLow}BriefVOList;
                    }

                    /**
                    * 持久化
                    *
                    * @param ${entityNameHumpFirstLow}From 需要保存的对象
                    * @return id，如果保存失败返回空字符串
                    * @throws ServiceException 业务层异常
                    */
                    @Override
                    public String save(${entityNameHump}From ${entityNameHumpFirstLow}From) throws ServiceException {
                    ${entityNameHump} ${entityNameHumpFirstLow} = new ${entityNameHump}();
                    try {
                    BeanUtils.copyProperties(${entityNameHumpFirstLow}From, ${entityNameHumpFirstLow}, "id");
                    } catch (Exception e) {
                    throw new ServiceException(e, ${entityNameHumpFirstLow}From);
                    }
                    boolean isSuccess = this.save(${entityNameHumpFirstLow});
                    if (!isSuccess){
                    return "";
                    }
                    return ${entityNameHumpFirstLow}.getId();
                    }

                    /**
                    * 完全更新
                    *
                    * @param ${entityNameHumpFirstLow}From 需要更新的对象
                    * @return 是否成功
                    * @throws ServiceException 业务层异常
                    */
                    @Override
                    public Boolean updateById(${entityNameHump}From ${entityNameHumpFirstLow}From) throws
                    ServiceException {
                    ${entityNameHump} ${entityNameHumpFirstLow} = new ${entityNameHump}();
                    try {
                    BeanUtils.copyProperties(${entityNameHumpFirstLow}From, ${entityNameHumpFirstLow});
                    } catch (Exception e) {
                    throw new ServiceException(e, ${entityNameHumpFirstLow}From);
                    }
                    if (StringUtils.isEmpty(${entityNameHumpFirstLow}.getId())){
                    return false;
                    }
                    return this.updateById(${entityNameHumpFirstLow});
                    }

                    /**
                    * 部分更新
                    *
                    * @param ${entityNameHumpFirstLow}From 需要更新的对象
                    * @return 是否成功
                    * @throws ServiceException 业务层异常
                    */
                    @Override
                    public Boolean updateSelectiveById(${entityNameHump}From ${entityNameHumpFirstLow}From) throws
                    ServiceException {
                    ${entityNameHump} ${entityNameHumpFirstLow} = new ${entityNameHump}();
                    try {
                    BeanUtils.copyProperties(${entityNameHumpFirstLow}From, ${entityNameHumpFirstLow});
                    } catch (Exception e) {
                    throw new ServiceException(e, ${entityNameHumpFirstLow}From);
                    }
                    if (StringUtils.isEmpty(${entityNameHumpFirstLow}.getId())){
                    return false;
                    }
                    ${entityNameHump} old = this.getModel(${entityNameHumpFirstLow}.getId());
                    try {
                    MyBeanUtils.copyBeanNotNull2Bean(${entityNameHumpFirstLow},old);
                    } catch (Exception e) {
                    throw new ServiceException(e, ${entityNameHumpFirstLow});
                    }
                    return this.updateById(old);
                    }


                    /**
                    * 根据id获取详情DetailVO
                    *
                    * @param ${entityNameHumpFirstLow}Id 对象id
                    * @return 详情DetailVO对象
                    * @throws ServiceException 业务层异常
                    */
                    @Override
                    public ${entityNameHump}DetailVO getDetailVO(String ${entityNameHumpFirstLow}Id) throws
                    ServiceException {
                    ${entityNameHump} ${entityNameHumpFirstLow};
                    try {
                    ${entityNameHumpFirstLow} = this.baseManager.getModel(${entityNameHumpFirstLow}Id);
                    } catch (DAOException e) {
                    throw new ServiceException(e, ${entityNameHumpFirstLow}Id);
                    }
                    return this.getDetailVO(${entityNameHumpFirstLow});
                    }


                    /**
                    * 简略对象条件查询
                    *
                    * @param query 查询条件
                    * @return 查询出来的简略对象
                    * @throws ServiceException 业务层异常
                    */
                    @Override
                    public ${entityNameHump}BriefPageVO getBriefPageVO(${entityNameHump}Query query) throws
                    ServiceException {
                    IPage<${entityNameHump}> page;
                    try {
                    page = this.baseManager.listModels(query);
                    } catch (DAOException e) {
                    throw new ServiceException(e, query);
                    }
                    List<${entityNameHump}> list = page.getRecords();
                    List
                    <${entityNameHump}BriefVO> voList = this.listBriefVOS(list);
                        return new ${entityNameHump}BriefPageVO(page, voList);
                        }

                        /**
                        * 详细对象条件查询
                        *
                        * @param query 查询条件
                        * @return 查询出来的详细对象
                        * @throws ServiceException 业务层异常
                        */
                        @Override
                        public ${entityNameHump}DetailPageVO getDetailPageVO(${entityNameHump}Query query) throws
                        ServiceException {
                        IPage<${entityNameHump}> page;
                        try {
                        page = this.baseManager.listModels(query);
                        } catch (DAOException e) {
                        throw new ServiceException(e, query);
                        }
                        List<${entityNameHump}> list = page.getRecords();
                        List
                        <${entityNameHump}DetailVO> voList = this.listDetailVOS(list);
                            return new ${entityNameHump}DetailPageVO(page, voList);
                            }


                            }
