package ${basePackage}.${entityNameWithOutUnderline}.controller;

import ${basePackage}.${entityNameWithOutUnderline}.from.${entityNameHump}From;
import ${basePackage}.${entityNameWithOutUnderline}.query.${entityNameHump}Query;
import ${basePackage}.${entityNameWithOutUnderline}.service.${entityNameHump}ServiceI;
import ${basePackage}.${entityNameWithOutUnderline}.vo.${entityNameHump}BriefPageVO;
import ${basePackage}.${entityNameWithOutUnderline}.vo.${entityNameHump}DetailPageVO;
import ${basePackage}.${entityNameWithOutUnderline}.vo.${entityNameHump};
import ${basePackage}.${entityNameWithOutUnderline}.vo.data.*;
import com.seed.core.controller.AppBaseController;
import com.seed.core.exception.ServiceException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;


/**
* @author ${author}
* @date ${date}
*/
@Slf4j
@RestController(value = "${entityNameHumpFirstLow}ApiController")
@Api(value = "${entityNameHumpFirstLow}Controller", tags = "${entityNameHumpFirstLow}Controller")
@RequestMapping("/app/v1/${entityNameHumpFirstLow}")
public class ${entityNameHump}Controller extends AppBaseController {

private final ${entityNameHump}ServiceI ${entityNameHumpFirstLow}Service;

@Autowired
public ${entityNameHump}Controller(@Qualifier(value = "${entityNameHumpFirstLow}ApiService") ${entityNameHump}ServiceI ${entityNameHumpFirstLow}Service) {
this.${entityNameHumpFirstLow}Service = ${entityNameHumpFirstLow}Service;
}


@RequestMapping(method = RequestMethod.POST)
@ResponseBody
@ApiOperation(value = "保存")
public ${entityNameHump}VO save(@RequestBody @ApiParam(value = "保存表单", required = true) ${entityNameHump}From ${entityNameHumpFirstLow}From) {
log.info("[api-v1]${entityNameHump}Controller.save,保存");

${entityNameHump}VO result = new ${entityNameHump}VO();
if (StringUtils.isEmpty(${entityNameHumpFirstLow}From)) {
result.setMsg("表单不能为空!");
result.setSuccess(false);
return result;
}
String id;
try {
id = ${entityNameHumpFirstLow}Service.save(${entityNameHumpFirstLow}From);
} catch (ServiceException e) {
log.error("[api-v1]${entityNameHump}Controller.save {}", e);
result.setSuccess(false);
result.setMsg("保存失败");
return result;
}
if (StringUtils.isEmpty(id)) {
result.setSuccess(false);
result.setMsg("保存失败");
return result;
}
${entityNameHump}DetailVO data;
try {
data = ${entityNameHumpFirstLow}Service.getDetailVO(id);
} catch (ServiceException e) {
log.error("[api-v1]${entityNameHump}Controller.save {}", e);
result.setSuccess(false);
result.setMsg("保存失败");
return result;
}
result.setData(data);
return result;
}


@RequestMapping(method = RequestMethod.PUT)
@ResponseBody
@ApiOperation(value = "完全更新,更新from中所有的值（空值也会被更新）")
public ${entityNameHump}VO update(@RequestBody @ApiParam(value = "更新表单", required = true) ${entityNameHump}From ${entityNameHumpFirstLow}From) {
log.info("[api-v1]${entityNameHump}Controller.update,完全更新");

${entityNameHump}VO result = new ${entityNameHump}VO();
if (StringUtils.isEmpty(${entityNameHumpFirstLow}From)) {
result.setMsg("更新信息不能为空!");
result.setSuccess(false);
return result;
}
if (StringUtils.isEmpty(${entityNameHumpFirstLow}From.getId())){
result.setMsg("id信息不能为空!");
result.setSuccess(false);
return result;
}
Boolean isSuccess;
try {
isSuccess = ${entityNameHumpFirstLow}Service.updateById(${entityNameHumpFirstLow}From);
} catch (ServiceException e) {
log.error("[api-v1]${entityNameHump}Controller.update {}", e);
result.setSuccess(false);
result.setMsg("更新失败");
return result;
}
if (!isSuccess) {
result.setSuccess(false);
result.setMsg("更新失败");
return result;
}
${entityNameHump}DetailVO data;
try {
data = ${entityNameHumpFirstLow}Service.getDetailVO(${entityNameHumpFirstLow}From.getId());
} catch (ServiceException e) {
log.error("[api-v1]${entityNameHump}Controller.update {}", e);
result.setSuccess(false);
result.setMsg("更新失败");
return result;
}
result.setData(data);
return result;
}


@RequestMapping(method = RequestMethod.PATCH)
@ResponseBody
@ApiOperation(value = "不完全更新,更新from中不为空的值（空值不会被更新）")
public ${entityNameHump}VO updateSelective(@RequestBody @ApiParam(value = "更新表单", required = true) ${entityNameHump}From ${entityNameHumpFirstLow}From) {
log.info("[api-v1]${entityNameHump}Controller.updateSelective,完全更新");

${entityNameHump}VO result = new ${entityNameHump}VO();
if (StringUtils.isEmpty(${entityNameHumpFirstLow}From)) {
result.setMsg("更新信息不能为空!");
result.setSuccess(false);
return result;
}
if (StringUtils.isEmpty(${entityNameHumpFirstLow}From.getId())){
result.setMsg("id信息不能为空!");
result.setSuccess(false);
return result;
}
Boolean isSuccess;
try {
isSuccess = ${entityNameHumpFirstLow}Service.updateSelectiveById(${entityNameHumpFirstLow}From);
} catch (ServiceException e) {
log.error("[api-v1]${entityNameHump}Controller.updateSelective {}", e);
result.setSuccess(false);
result.setMsg("更新失败");
return result;
}
if (!isSuccess) {
result.setSuccess(false);
result.setMsg("更新失败");
return result;
}
${entityNameHump}DetailVO data;
try {
data = ${entityNameHumpFirstLow}Service.getDetailVO(${entityNameHumpFirstLow}From.getId());
} catch (ServiceException e) {
log.error("[api-v1]${entityNameHump}Controller.update {}", e);
result.setSuccess(false);
result.setMsg("更新失败");
return result;
}
result.setData(data);
return result;
}

@RequestMapping(value = "/{ids}", method = RequestMethod.DELETE)
@ResponseBody
@ApiOperation(value = "删除,支持ids")
public ${entityNameHump}VO delete(@PathVariable @ApiParam(value = "主键id", required = true) String ids) {
log.info("[api-v1]${entityNameHump}Controller.delete,删除");

${entityNameHump}VO result = new ${entityNameHump}VO();
if (StringUtils.isEmpty(ids)) {
result.setMsg("id不能为空!");
result.setSuccess(false);
return result;
}
Boolean isSuccess;
try {
isSuccess = ${entityNameHumpFirstLow}Service.deleteByIds(ids);
} catch (ServiceException e) {
log.error("[api-v1]${entityNameHump}Controller.delete {}", e);
result.setSuccess(false);
result.setMsg("删除失败");
return result;
}
if (!isSuccess) {
result.setSuccess(false);
result.setMsg("删除失败");
return result;
}
return result;
}


@RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
@ResponseBody
@ApiOperation(value = "根据id获取${tableComment}详细信息")
public ${entityNameHump}VO get${entityNameHump}VO(@PathVariable @ApiParam(value = "主键id", required = true) String id) {
log.info("[api-v1]${entityNameHump}Controller.get${entityNameHump}VO接口,根据id获取${tableComment}详细信息");

${entityNameHump}VO result  = new ${entityNameHump}VO();
if (StringUtils.isEmpty(id)) {
result.setMsg("id不能为空!");
result.setSuccess(false);
return result;
}

${entityNameHump}DetailVO data;
try {
data  = ${entityNameHumpFirstLow}Service.getDetailVO(id);
} catch (ServiceException e) {
log.error("[api-v1]${entityNameHump}Controller.get${entityNameHump}VO接口 {}",e);
result.setSuccess(false);
result.setMsg("查询失败");
return result;
}
result.setData(data);
return result;
}

@RequestMapping(value = "/brief", method = RequestMethod.POST)
@ResponseBody
@ApiOperation(value = "根据查询条件查询")
public ${entityNameHump}BriefPageVO getBriefPageVO(@RequestBody @ApiParam(value = "查询条件", required = true) ${entityNameHump}Query query) {
log.info("[api-v1]${entityNameHump}Controller.getBriefPageVO,根据查询条件查询");

${entityNameHump}BriefPageVO result = new ${entityNameHump}BriefPageVO();
if (StringUtils.isEmpty(query)) {
query = new ${entityNameHump}Query();
}

try {
result = ${entityNameHumpFirstLow}Service.getBriefPageVO(query);
} catch (ServiceException e) {
log.error("[api-v1]${entityNameHump}Controller.getBriefPageVO {}", e);
result.setSuccess(false);
result.setMsg("查询失败");
return result;
}

return result;
}


@RequestMapping(value = "/detail", method = RequestMethod.POST)
@ResponseBody
@ApiOperation(value = "根据查询条件查询")
public ${entityNameHump}DetailPageVO getDetailPageVO(@RequestBody @ApiParam(value = "查询条件", required = true) ${entityNameHump}Query query) {
log.info("[api-v1]${entityNameHump}Controller.getDetailPageVO,根据查询条件查询");

${entityNameHump}DetailPageVO result = new ${entityNameHump}DetailPageVO();
if (StringUtils.isEmpty(query)) {
query = new ${entityNameHump}Query();
}

try {
result = ${entityNameHumpFirstLow}Service.getDetailPageVO(query);
} catch (ServiceException e) {
log.error("[api-v1]${entityNameHump}Controller.getDetailPageVO {}", e);
result.setSuccess(false);
result.setMsg("查询失败");
return result;
}

return result;
}

}
