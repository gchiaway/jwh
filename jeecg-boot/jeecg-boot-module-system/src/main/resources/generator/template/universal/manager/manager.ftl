package ${basePackage}.${entityNameWithOutUnderline}.manager;

import com.baomidou.mybatisplus.core.metadata.IPage;
import ${basePackage}.${entityNameWithOutUnderline}.query.${entityNameHump}Query;
import ${bussPackage}.${entityNameWithOutUnderline}.entity.${entityNameHump};
import com.seed.core.exception.DAOException;
import com.seed.core.manager.Manager;

/**H
* @author ${author}
* @date ${date}
*/
public interface ${entityNameHump}ManagerI extends Manager<${entityNameHump}>{
/**
* 查询出所有符合的数据
*
* @param query 查询条件
* @return model集合
* @throws DAOException 持久层异常
*/
IPage<${entityNameHump}> listModels(${entityNameHump}Query query) throws DAOException;
}
