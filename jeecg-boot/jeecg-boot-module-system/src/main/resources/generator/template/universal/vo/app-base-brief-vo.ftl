package ${basePackage}.${entityNameWithOutUnderline}.vo.data;

import com.seed.core.vo.data.BaseData;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
* ${tableComment}的BriefVO对象
*
* @author ${author}
* @date ${date}
*/
@Getter
@Setter
@ToString
public class ${entityNameHump}BriefVO extends BaseData {

@ApiModelProperty(value = "主键", example = "1")
private String id;
}