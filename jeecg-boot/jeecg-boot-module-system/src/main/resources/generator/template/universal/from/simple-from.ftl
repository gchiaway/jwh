package ${basePackage}.${entityNameWithOutUnderline}.from;

import com.seed.core.from.BaseFrom;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
* ${tableComment}的From对象
*
* @author ${author}
* @date ${date}
*/
@Getter
@Setter
@ToString
public class ${entityNameHump}From extends BaseFrom {

/**
* 主键
*/
@ApiModelProperty(value = "主键", required = true,  example = "1")
private String id;

}