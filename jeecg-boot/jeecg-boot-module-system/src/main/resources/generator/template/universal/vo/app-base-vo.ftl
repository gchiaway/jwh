package ${basePackage}.${entityNameWithOutUnderline}.vo;

import ${basePackage}.${entityNameWithOutUnderline}.vo.data.${entityNameHump}DetailVO;
import com.seed.core.vo.BaseModelVO;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
* ${tableComment}的VO对象
*
* @author ${author}
* @date ${date}
*/
@Getter
@Setter
@ToString
public class ${entityNameHump}VO extends BaseModelVO
<${entityNameHump}DetailVO> {
    public ${entityNameHump}VO() {
    }

    public ${entityNameHump}VO(${entityNameHump}DetailVO data) {
    super(data);
    }
    }