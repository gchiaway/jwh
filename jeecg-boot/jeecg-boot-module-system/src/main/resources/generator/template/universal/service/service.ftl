package ${basePackage}.${entityNameWithOutUnderline}.service;

import ${basePackage}.${entityNameWithOutUnderline}.from.${entityNameHump}From;
import ${basePackage}.${entityNameWithOutUnderline}.query.${entityNameHump}Query;
import ${basePackage}.${entityNameWithOutUnderline}.vo.${entityNameHump}BriefPageVO;
import ${basePackage}.${entityNameWithOutUnderline}.vo.${entityNameHump}DetailPageVO;
import ${basePackage}.${entityNameWithOutUnderline}.vo.data.${entityNameHump}DetailVO;
import ${bussPackage}.${entityNameWithOutUnderline}.entity.${entityNameHump};
import com.seed.core.exception.ServiceException;
import com.seed.core.service.Service;

/**
* @author ${author}
* @date ${date}
*/
public interface ${entityNameHump}ServiceI extends Service<${entityNameHump}>{

/**
* 持久化
*
* @param ${entityNameHumpFirstLow}From 需要保存的对象
* @return id，如果保存失败返回空字符串
* @throws ServiceException 业务层异常
*/
String save(${entityNameHump}From ${entityNameHumpFirstLow}From) throws ServiceException;

/**
* 完全更新
*
* @param ${entityNameHumpFirstLow}From 需要更新的对象
* @return 是否成功
* @throws ServiceException 业务层异常
*/
Boolean updateById(${entityNameHump}From ${entityNameHumpFirstLow}From) throws ServiceException;

/**
* 部分更新
*
* @param ${entityNameHumpFirstLow}From 需要更新的对象
* @return 是否成功
* @throws ServiceException 业务层异常
*/
Boolean updateSelectiveById(${entityNameHump}From ${entityNameHumpFirstLow}From) throws ServiceException;

/**
* 根据id获取详情VO
*
* @param ${entityNameHumpFirstLow}Id 对象id
* @return 详情VO对象
* @throws ServiceException 业务层异常
*/
${entityNameHump}DetailVO getDetailVO(String ${entityNameHumpFirstLow}Id) throws ServiceException;


/**
* 简略对象条件查询
*
* @param query 查询条件
* @return 查询出来的简略对象
* @throws ServiceException 业务层异常
*/
${entityNameHump}BriefPageVO getBriefPageVO(${entityNameHump}Query query) throws ServiceException;

/**
* 详细对象条件查询
*
* @param query 查询条件
* @return 查询出来的详细对象
* @throws ServiceException 业务层异常
*/
${entityNameHump}DetailPageVO getDetailPageVO(${entityNameHump}Query query) throws ServiceException;
}
