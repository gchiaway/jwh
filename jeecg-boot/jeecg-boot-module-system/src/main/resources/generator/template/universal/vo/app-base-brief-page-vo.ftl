package ${basePackage}.${entityNameWithOutUnderline}.vo;

import com.baomidou.mybatisplus.core.metadata.IPage;
import ${basePackage}.${entityNameWithOutUnderline}.vo.data.${entityNameHump}BriefVO;
import com.seed.core.vo.BasePageVO;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
* ${tableComment}的BriefPage对象
*
* @author ${author}
* @date ${date}
*/
@Getter
@Setter
@ToString
public class ${entityNameHump}BriefPageVO extends BasePageVO
<${entityNameHump}BriefVO>{
    public ${entityNameHump}BriefPageVO() {

    }

    public ${entityNameHump}BriefPageVO(IPage page, List
    <${entityNameHump}BriefVO> list) {
        super(page, list);
        }
        }