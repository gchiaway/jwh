package ${basePackage}.${entityNameWithOutUnderline}.query;

import com.seed.core.query.BaseQuery;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
* ${tableComment}Query
* @author ${author}
* @date ${date}
*/
@Getter
@Setter
@ToString
public class ${entityNameHump}Query extends BaseQuery {

}