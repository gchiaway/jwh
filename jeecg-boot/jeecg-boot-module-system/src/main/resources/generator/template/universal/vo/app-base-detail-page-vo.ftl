package ${basePackage}.${entityNameWithOutUnderline}.vo;

import com.baomidou.mybatisplus.core.metadata.IPage;
import ${basePackage}.${entityNameWithOutUnderline}.vo.data.${entityNameHump}DetailVO;
import com.seed.core.vo.BasePageVO;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
* ${tableComment}的DetailPage对象
*
* @author ${author}
* @date ${date}
*/
@Getter
@Setter
@ToString
public class ${entityNameHump}DetailPageVO extends BasePageVO
<${entityNameHump}DetailVO>{
    public ${entityNameHump}DetailPageVO() {

    }

    public ${entityNameHump}DetailPageVO(IPage page, List
    <${entityNameHump}DetailVO> list) {
        super(page, list);
        }
        }