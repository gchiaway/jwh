package ${basePackage}.${entityNameWithOutUnderline}.manager.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import ${basePackage}.${entityNameWithOutUnderline}.manager.${entityNameHump}ManagerI;
import ${basePackage}.${entityNameWithOutUnderline}.query.${entityNameHump}Query;
import ${bussPackage}.${entityNameWithOutUnderline}.entity.${entityNameHump};
import ${bussPackage}.${entityNameWithOutUnderline}.mapper.${entityNameHump}Mapper;
import com.seed.core.exception.DAOException;
import com.seed.core.manager.impl.AbstractManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
* @author ${author}
* @date ${date}
*/
@Slf4j
@Service("${entityNameHumpFirstLow}ApiManager")
@Transactional(rollbackFor = DAOException.class)
public class ${entityNameHump}ManagerImpl extends AbstractManager
<${entityNameHump}Mapper, ${entityNameHump}> implements ${entityNameHump}ManagerI {

/**
* 条件查询对象
*
* @param query 查询条件
* @return model
*/
private ${entityNameHump} getQueryModel(${entityNameHump}Query query) {
${entityNameHump} ${entityNameHumpFirstLow} = new ${entityNameHump}();
BeanUtils.copyProperties(query, ${entityNameHumpFirstLow});
return ${entityNameHumpFirstLow};
}

/**
* 查询出所有符合的数据
*
* @param query 查询条件
* @return model集合
* @throws DAOException 持久层异常
*/
@Override
public IPage<${entityNameHump}> listModels(${entityNameHump}Query query) throws DAOException {
${entityNameHump} ${entityNameHumpFirstLow} = this.getQueryModel(query);
try {
Page<${entityNameHump}> page = new Page<>(query.getQueryPage(), query.getQuerySize());
return this.listModels(${entityNameHumpFirstLow}, page);
} catch (DAOException e) {
throw new DAOException(e, ${entityNameHumpFirstLow}, query);
}
}
}