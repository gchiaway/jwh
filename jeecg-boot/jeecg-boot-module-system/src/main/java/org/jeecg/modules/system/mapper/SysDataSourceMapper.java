package org.jeecg.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.seed.core.table.entity.DataBaseTable;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.system.entity.SysDataSource;

import java.util.List;

/**
 * @Description: 多数据源管理
 * @Author: jeecg-boot
 * @Date: 2019-12-25
 * @Version: V1.0
 */
public interface SysDataSourceMapper extends BaseMapper<SysDataSource> {

    /**
     * 根据SQL查询表信息
     *
     * @param sql 查询SQL
     * @return 表信息集合
     */
    List<DataBaseTable> listDataBaseTablesBySQL(@Param(value = "sql") String sql);
}
