package com.business.app.wechat.core.entity;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 用户基本信息
 *
 * @author gchiaway
 * 日期: 2019-06-29
 * 时间: 20:51
 */
@Getter
@Setter
@ToString
public class UserInfoEntity {
    /**
     * openId
     */
    private String openId;
    /**
     * 头像地址
     */
    private String headImgUrl;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 性别
     */
    private int sex;
}
