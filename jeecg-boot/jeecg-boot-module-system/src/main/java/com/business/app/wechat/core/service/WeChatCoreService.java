package com.business.app.wechat.core.service;

import com.business.app.wechat.core.entity.UserInfoEntity;

/**
 * 微信操作核心类
 *
 * @author gchiaway
 * 日期: 2020-04-04
 * 时间: 23:38
 */
public interface WeChatCoreService {

    /**
     * 更新access_token 和 jsApi_ticket
     *
     * @return 是否成功
     */
    Boolean updateTokenAndTicket();

    /**
     * 获取基础接口access_token
     *
     * @return access_token
     */
    String getAccessToken();

    /**
     * 获取jsApi_ticket
     *
     * @return jsApi_ticket
     */
    String getJsApiTicket();

    /**
     * 通过网页授权获取openId
     *
     * @param code 微信code
     * @return openId
     */
    String getOpenId(String code);

    /**
     * 通过openId获取用户信息
     *
     * @param openId openId
     * @return 用户信息
     */
    UserInfoEntity getUserInfo(String openId);
}
