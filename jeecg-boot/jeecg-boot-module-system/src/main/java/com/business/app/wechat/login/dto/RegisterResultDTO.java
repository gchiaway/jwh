package com.business.app.wechat.login.dto;

import com.seed.core.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author gchiaway
 * 日期: 2020-05-11
 * 时间: 9:53
 */
@Getter
@Setter
@ToString
public class RegisterResultDTO extends BaseDTO {

}
