package com.business.app.wechat.login.dto;

import com.seed.core.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author gchiaway
 * 日期: 2020-05-12
 * 时间: 10:33
 */
@Getter
@Setter
@ToString
public class RegisterDTO extends BaseDTO {
    /**
     * 头像
     */
    private String avatar;
    /**
     * 性别（1：男 2：女）
     */
    private Integer sex;
    /**
     * 微信openid
     */
    private String openid;
    /**
     * 微信昵称
     */
    private String nickname;
}
