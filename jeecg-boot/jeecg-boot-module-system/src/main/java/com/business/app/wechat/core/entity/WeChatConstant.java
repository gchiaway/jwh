package com.business.app.wechat.core.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 微信常量
 *
 * @author gchiaway
 */
@Setter
@Getter
@Component
public class WeChatConstant {
    /**
     * APPID
     */
    @Value("${wechat.APPID}")
    private String APPID;
    /**
     * APPSECRET
     */
    @Value("${wechat.APPSECRET}")
    private String APPSECRET;
}
