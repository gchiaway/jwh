package com.business.app.wechat.core.url;

/**
 * 微信请求地址
 *
 * @author gchiaway
 * 日期: 2020-04-05
 * 时间: 0:01
 */
public enum UrlEnum {
    /**
     * 获取ACCESS_TOKEN
     */
    GET_ACCESS_TOKEN("获取ACCESS_TOKEN", "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET"),
    /**
     * 通过code获取网页授权的access_token
     */
    GET_WEB_ACCESS_TOKEN("获取网页授权的ACCESS_TOKEN", "https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code"),
    /**
     * 获取jsApi_ticket
     */
    GET_JsAPI_TICKET("获取jsApi_ticket", "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=ACCESS_TOKEN&type=jsapi"),
    /**
     * 获取openid
     */
    GET_OPENID("获取openid", "https://api.weixin.qq.com/sns/jscode2session?appid=APPID&secret=APPSECRET&js_code=CODE&grant_type=authorization_code"),
    /**
     * 通过openid获取用户的信息
     */
    GET_USER_INFO("获取用户的信息", "https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN"),
    ;

    /**
     * 链接名称
     */
    private String name;

    /**
     * 地址
     */
    private String url;

    UrlEnum(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
