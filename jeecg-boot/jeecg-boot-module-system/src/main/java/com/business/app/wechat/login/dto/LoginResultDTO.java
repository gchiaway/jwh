package com.business.app.wechat.login.dto;

import com.seed.core.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author gchiaway
 * 日期: 2020-04-05
 * 时间: 1:44
 */
@Getter
@Setter
@ToString
public class LoginResultDTO extends BaseDTO {
    public LoginResultDTO() {
        this.setRegistered(true);
    }

    /**
     * 是否注册了
     */
    private Boolean registered;
    /**
     * openId
     */
    private String openid;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 登录凭证
     */
    private String token;

}
