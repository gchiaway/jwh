package com.business.app.wechat.core.task;

import com.business.app.wechat.core.service.WeChatCoreService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

/**
 * 使用quartz来调度任务
 *
 * @author gchiaway
 */
@Slf4j
public class UpdateTokenAndTicketJob implements Job {
    /**
     * 微信service
     */
    @Autowired
    private WeChatCoreService weChatCoreService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.info(new Date().toString() + ": 开始更新 AccessToken 与 JsApi Ticket");
        Boolean isSuccess = weChatCoreService.updateTokenAndTicket();
        if (!isSuccess) {
            log.warn(new Date().toString() + ":更新 AccessToken 与 JsApi Ticket 失败");
        }
    }
}

