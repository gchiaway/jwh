package com.business.app.wechat.login.service;

import com.business.app.wechat.core.service.WeChatCoreService;
import com.business.app.wechat.login.dto.LoginResultDTO;
import com.business.app.wechat.login.dto.RegisterDTO;
import com.business.app.wechat.login.dto.RegisterResultDTO;
import org.jeecg.modules.system.entity.SysUser;

/**
 * @author gchiaway
 * 日期: 2019-04-06
 * 时间: 16:10
 */
public interface WeChatLoginService extends WeChatCoreService {

    /**
     * 注册
     *
     * @param registerDTO 注册对象
     * @return 注册结果
     */
    RegisterResultDTO register(RegisterDTO registerDTO);

    /**
     * 根据openid获取用户信息
     *
     * @param openId openid
     * @return 用户信息
     */
    SysUser getUser(String openId);

    /**
     * 通过openid登录
     *
     * @param openId openid
     * @return 登录结果
     */
    LoginResultDTO login(String openId);
}
