package com.business.app.wechat.login.vo;

import com.seed.core.vo.BaseVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author gchiaway
 * 日期: 2019-06-29
 * 时间: 21:01
 */
@Getter
@Setter
@ToString
public class LoginResultVO extends BaseVO {
    public LoginResultVO() {
        this.setRegistered(true);
    }

    /**
     * 是否注册了
     */
    @ApiModelProperty(value = "是否注册", example = "true")
    private Boolean registered;
    /**
     * openId
     */
    @ApiModelProperty(value = "openId", example = "456489476")
    private String openid;
    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id", example = "qwe46546849")
    private String userId;
    /**
     * 登录凭证
     */
    @ApiModelProperty(value = "登录凭证token", example = "seed7984765")
    private String token;
}
