package com.business.app.wechat.core.entity;

/**
 * @author gchiaway
 * 日期: 2020-05-10
 * 时间: 19:59
 */
public enum WeChatEnum {
    /**
     * accessToken
     */
    ACCESSTOKEN("wechat.accessToken"),
    /**
     * jsApi ticket 凭证
     */
    JSAPITICKET("wechat.jsApiTicket"),
    ;

    /**
     *
     */
    private String key;

    WeChatEnum(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
