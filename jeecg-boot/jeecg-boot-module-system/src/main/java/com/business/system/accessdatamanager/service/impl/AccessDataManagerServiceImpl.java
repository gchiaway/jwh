package com.business.system.accessdatamanager.service.impl;

import com.business.system.accessdatamanager.entity.AccessDataManager;
import com.business.system.accessdatamanager.mapper.AccessDataManagerMapper;
import com.business.system.accessdatamanager.service.IAccessDataManagerService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 数据管理
 * @Author: jeecg-boot
 * @Date:   2020-05-12
 * @Version: V1.0
 */
@Service
public class AccessDataManagerServiceImpl extends ServiceImpl<AccessDataManagerMapper, AccessDataManager> implements IAccessDataManagerService {

}
