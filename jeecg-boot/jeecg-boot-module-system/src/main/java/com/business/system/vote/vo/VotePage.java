package com.business.system.vote.vo;

import com.business.system.vote.entity.VoteOption;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelCollection;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * @Description: 投票
 * @Author: jeecg-boot
 * @Date:   2020-05-13
 * @Version: V1.0
 */
@Data
@ApiModel(value="votePage对象", description="投票")
public class VotePage {

	/**主键*/
	@ApiModelProperty(value = "主键")
	private String id;
	/**活动名称*/
	@Excel(name = "活动名称", width = 15)
	@ApiModelProperty(value = "活动名称")
	private String name;
	/**开始时间*/
	@Excel(name = "开始时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "开始时间")
	private Date startTime;
	/**结束时间*/
	@Excel(name = "结束时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "结束时间")
	private Date endTime;
	/**是否可评论*/
	@Excel(name = "是否可评论", width = 15)
	@ApiModelProperty(value = "是否可评论")
	private String messageType;
	/**每人每日可投票数*/
	@Excel(name = "每人每日可投票数", width = 15)
	@ApiModelProperty(value = "每人每日可投票数")
	private Integer voteNum;
	/**每人可投总票数*/
	@Excel(name = "每人可投总票数", width = 15)
	@ApiModelProperty(value = "每人可投总票数")
	private Integer voteCount;
	/**封面图*/
	@Excel(name = "封面图", width = 15)
	@ApiModelProperty(value = "封面图")
	private String img;
	/**活动规则*/
	@Excel(name = "活动规则", width = 15)
	@ApiModelProperty(value = "活动规则")
	private String rule;
	/**活动是否启用*/
	@Excel(name = "活动是否启用", width = 15)
	@ApiModelProperty(value = "活动是否启用")
	private String enableStatus;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "创建日期")
	private Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "更新日期")
	private Date updateTime;
	
	@ExcelCollection(name="投票选项")
	@ApiModelProperty(value = "投票选项")
	private List<VoteOption> voteOptionList;
	
}
