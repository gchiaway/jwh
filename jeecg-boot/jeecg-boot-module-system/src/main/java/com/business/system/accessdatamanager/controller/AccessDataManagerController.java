package com.business.system.accessdatamanager.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.business.system.accessdatamanager.entity.AccessDataManager;
import com.business.system.accessdatamanager.service.IAccessDataManagerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

 /**
 * @Description: 数据管理
 * @Author: jeecg-boot
 * @Date:   2020-05-12
 * @Version: V1.0
 */
@Api(tags="数据管理")
@RestController
@RequestMapping("/accessdatamanager/accessDataManager")
@Slf4j
public class AccessDataManagerController extends JeecgController<AccessDataManager, IAccessDataManagerService> {
	@Autowired
	private IAccessDataManagerService accessDataManagerService;
	
	/**
	 * 分页列表查询
	 *
	 * @param accessDataManager
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "数据管理-分页列表查询")
	@ApiOperation(value="数据管理-分页列表查询", notes="数据管理-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(AccessDataManager accessDataManager,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<AccessDataManager> queryWrapper = QueryGenerator.initQueryWrapper(accessDataManager, req.getParameterMap());
		Page<AccessDataManager> page = new Page<AccessDataManager>(pageNo, pageSize);
		IPage<AccessDataManager> pageList = accessDataManagerService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param accessDataManager
	 * @return
	 */
	@AutoLog(value = "数据管理-添加")
	@ApiOperation(value="数据管理-添加", notes="数据管理-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody AccessDataManager accessDataManager) {
		accessDataManagerService.save(accessDataManager);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param accessDataManager
	 * @return
	 */
	@AutoLog(value = "数据管理-编辑")
	@ApiOperation(value="数据管理-编辑", notes="数据管理-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody AccessDataManager accessDataManager) {
		accessDataManagerService.updateById(accessDataManager);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "数据管理-通过id删除")
	@ApiOperation(value="数据管理-通过id删除", notes="数据管理-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		accessDataManagerService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "数据管理-批量删除")
	@ApiOperation(value="数据管理-批量删除", notes="数据管理-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.accessDataManagerService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "数据管理-通过id查询")
	@ApiOperation(value="数据管理-通过id查询", notes="数据管理-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		AccessDataManager accessDataManager = accessDataManagerService.getById(id);
		if(accessDataManager==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(accessDataManager);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param accessDataManager
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, AccessDataManager accessDataManager) {
        return super.exportXls(request, accessDataManager, AccessDataManager.class, "数据管理");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, AccessDataManager.class);
    }

}
