package com.business.system.membermanager.service.impl;

import com.business.system.membermanager.entity.MemberManager;
import com.business.system.membermanager.mapper.MemberManagerMapper;
import com.business.system.membermanager.service.IMemberManagerService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 成员管理
 * @Author: jeecg-boot
 * @Date:   2020-05-12
 * @Version: V1.0
 */
@Service
public class MemberManagerServiceImpl extends ServiceImpl<MemberManagerMapper, MemberManager> implements IMemberManagerService {

}
