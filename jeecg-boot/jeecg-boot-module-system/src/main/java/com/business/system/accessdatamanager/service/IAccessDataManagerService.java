package com.business.system.accessdatamanager.service;

import com.business.system.accessdatamanager.entity.AccessDataManager;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 数据管理
 * @Author: jeecg-boot
 * @Date:   2020-05-12
 * @Version: V1.0
 */
public interface IAccessDataManagerService extends IService<AccessDataManager> {

}
