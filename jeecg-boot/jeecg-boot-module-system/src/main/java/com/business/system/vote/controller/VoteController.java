package com.business.system.vote.controller;

import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.vo.LoginUser;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import com.business.system.vote.entity.VoteOption;
import com.business.system.vote.entity.Vote;
import com.business.system.vote.vo.VotePage;
import com.business.system.vote.service.IVoteService;
import com.business.system.vote.service.IVoteOptionService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 投票
 * @Author: jeecg-boot
 * @Date:   2020-05-13
 * @Version: V1.0
 */
@Api(tags="投票")
@RestController
@RequestMapping("/vote/vote")
@Slf4j
public class VoteController {
	@Autowired
	private IVoteService voteService;
	@Autowired
	private IVoteOptionService voteOptionService;
	
	/**
	 * 分页列表查询
	 *
	 * @param vote
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "投票-分页列表查询")
	@ApiOperation(value="投票-分页列表查询", notes="投票-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(Vote vote,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<Vote> queryWrapper = QueryGenerator.initQueryWrapper(vote, req.getParameterMap());
		Page<Vote> page = new Page<Vote>(pageNo, pageSize);
		IPage<Vote> pageList = voteService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param votePage
	 * @return
	 */
	@AutoLog(value = "投票-添加")
	@ApiOperation(value="投票-添加", notes="投票-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody VotePage votePage) {
		Vote vote = new Vote();
		BeanUtils.copyProperties(votePage, vote);
		voteService.saveMain(vote, votePage.getVoteOptionList());
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param votePage
	 * @return
	 */
	@AutoLog(value = "投票-编辑")
	@ApiOperation(value="投票-编辑", notes="投票-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody VotePage votePage) {
		Vote vote = new Vote();
		BeanUtils.copyProperties(votePage, vote);
		Vote voteEntity = voteService.getById(vote.getId());
		if(voteEntity==null) {
			return Result.error("未找到对应数据");
		}
		voteService.updateMain(vote, votePage.getVoteOptionList());
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "投票-通过id删除")
	@ApiOperation(value="投票-通过id删除", notes="投票-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		voteService.delMain(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "投票-批量删除")
	@ApiOperation(value="投票-批量删除", notes="投票-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.voteService.delBatchMain(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "投票-通过id查询")
	@ApiOperation(value="投票-通过id查询", notes="投票-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		Vote vote = voteService.getById(id);
		if(vote==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(vote);

	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "投票选项通过主表ID查询")
	@ApiOperation(value="投票选项主表ID查询", notes="投票选项-通主表ID查询")
	@GetMapping(value = "/queryVoteOptionByMainId")
	public Result<?> queryVoteOptionListByMainId(@RequestParam(name="id",required=true) String id) {
		List<VoteOption> voteOptionList = voteOptionService.selectByMainId(id);
		return Result.ok(voteOptionList);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param vote
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, Vote vote) {
      // Step.1 组装查询条件查询数据
      QueryWrapper<Vote> queryWrapper = QueryGenerator.initQueryWrapper(vote, request.getParameterMap());
      LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

      //Step.2 获取导出数据
      List<Vote> queryList = voteService.list(queryWrapper);
      // 过滤选中数据
      String selections = request.getParameter("selections");
      List<Vote> voteList = new ArrayList<Vote>();
      if(oConvertUtils.isEmpty(selections)) {
          voteList = queryList;
      }else {
          List<String> selectionList = Arrays.asList(selections.split(","));
          voteList = queryList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
      }

      // Step.3 组装pageList
      List<VotePage> pageList = new ArrayList<VotePage>();
      for (Vote main : voteList) {
          VotePage vo = new VotePage();
          BeanUtils.copyProperties(main, vo);
          List<VoteOption> voteOptionList = voteOptionService.selectByMainId(main.getId());
          vo.setVoteOptionList(voteOptionList);
          pageList.add(vo);
      }

      // Step.4 AutoPoi 导出Excel
      ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
      mv.addObject(NormalExcelConstants.FILE_NAME, "投票列表");
      mv.addObject(NormalExcelConstants.CLASS, VotePage.class);
      mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("投票数据", "导出人:"+sysUser.getRealname(), "投票"));
      mv.addObject(NormalExcelConstants.DATA_LIST, pageList);
      return mv;
    }

    /**
    * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
      Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
      for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
          MultipartFile file = entity.getValue();// 获取上传文件对象
          ImportParams params = new ImportParams();
          params.setTitleRows(2);
          params.setHeadRows(1);
          params.setNeedSave(true);
          try {
              List<VotePage> list = ExcelImportUtil.importExcel(file.getInputStream(), VotePage.class, params);
              for (VotePage page : list) {
                  Vote po = new Vote();
                  BeanUtils.copyProperties(page, po);
                  voteService.saveMain(po, page.getVoteOptionList());
              }
              return Result.ok("文件导入成功！数据行数:" + list.size());
          } catch (Exception e) {
              log.error(e.getMessage(),e);
              return Result.error("文件导入失败:"+e.getMessage());
          } finally {
              try {
                  file.getInputStream().close();
              } catch (IOException e) {
                  e.printStackTrace();
              }
          }
      }
      return Result.ok("文件导入失败！");
    }

}
