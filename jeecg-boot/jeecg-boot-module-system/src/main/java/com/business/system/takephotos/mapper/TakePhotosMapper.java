package com.business.system.takephotos.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.business.system.takephotos.entity.TakePhotos;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 随手拍
 * @Author: jeecg-boot
 * @Date:   2020-05-13
 * @Version: V1.0
 */
public interface TakePhotosMapper extends BaseMapper<TakePhotos> {

}
