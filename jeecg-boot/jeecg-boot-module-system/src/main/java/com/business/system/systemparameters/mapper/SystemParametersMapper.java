package com.business.system.systemparameters.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.business.system.systemparameters.entity.SystemParameters;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 系统参数
 * @Author: jeecg-boot
 * @Date:   2020-05-12
 * @Version: V1.0
 */
public interface SystemParametersMapper extends BaseMapper<SystemParameters> {

}
