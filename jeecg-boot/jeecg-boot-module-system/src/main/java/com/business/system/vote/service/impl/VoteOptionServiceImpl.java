package com.business.system.vote.service.impl;

import com.business.system.vote.entity.VoteOption;
import com.business.system.vote.mapper.VoteOptionMapper;
import com.business.system.vote.service.IVoteOptionService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 投票选项
 * @Author: jeecg-boot
 * @Date:   2020-05-13
 * @Version: V1.0
 */
@Service
public class VoteOptionServiceImpl extends ServiceImpl<VoteOptionMapper, VoteOption> implements IVoteOptionService {
	
	@Autowired
	private VoteOptionMapper voteOptionMapper;
	
	@Override
	public List<VoteOption> selectByMainId(String mainId) {
		return voteOptionMapper.selectByMainId(mainId);
	}
}
