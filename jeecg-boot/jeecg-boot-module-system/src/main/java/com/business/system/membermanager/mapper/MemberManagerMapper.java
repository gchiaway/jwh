package com.business.system.membermanager.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.business.system.membermanager.entity.MemberManager;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 成员管理
 * @Author: jeecg-boot
 * @Date:   2020-05-12
 * @Version: V1.0
 */
public interface MemberManagerMapper extends BaseMapper<MemberManager> {

}
