package com.business.system.membermanager.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import com.business.system.membermanager.entity.MemberManager;
import com.business.system.membermanager.service.IMemberManagerService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 成员管理
 * @Author: jeecg-boot
 * @Date:   2020-05-12
 * @Version: V1.0
 */
@Api(tags="成员管理")
@RestController
@RequestMapping("/membermanager/memberManager")
@Slf4j
public class MemberManagerController extends JeecgController<MemberManager, IMemberManagerService> {
	@Autowired
	private IMemberManagerService memberManagerService;
	
	/**
	 * 分页列表查询
	 *
	 * @param memberManager
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "成员管理-分页列表查询")
	@ApiOperation(value="成员管理-分页列表查询", notes="成员管理-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MemberManager memberManager,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MemberManager> queryWrapper = QueryGenerator.initQueryWrapper(memberManager, req.getParameterMap());
		Page<MemberManager> page = new Page<MemberManager>(pageNo, pageSize);
		IPage<MemberManager> pageList = memberManagerService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param memberManager
	 * @return
	 */
	@AutoLog(value = "成员管理-添加")
	@ApiOperation(value="成员管理-添加", notes="成员管理-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MemberManager memberManager) {
		memberManagerService.save(memberManager);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param memberManager
	 * @return
	 */
	@AutoLog(value = "成员管理-编辑")
	@ApiOperation(value="成员管理-编辑", notes="成员管理-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MemberManager memberManager) {
		memberManagerService.updateById(memberManager);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "成员管理-通过id删除")
	@ApiOperation(value="成员管理-通过id删除", notes="成员管理-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		memberManagerService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "成员管理-批量删除")
	@ApiOperation(value="成员管理-批量删除", notes="成员管理-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.memberManagerService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "成员管理-通过id查询")
	@ApiOperation(value="成员管理-通过id查询", notes="成员管理-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MemberManager memberManager = memberManagerService.getById(id);
		if(memberManager==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(memberManager);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param memberManager
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MemberManager memberManager) {
        return super.exportXls(request, memberManager, MemberManager.class, "成员管理");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MemberManager.class);
    }

}
