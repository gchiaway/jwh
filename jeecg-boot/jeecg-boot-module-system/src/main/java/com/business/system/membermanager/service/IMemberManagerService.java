package com.business.system.membermanager.service;

import com.business.system.membermanager.entity.MemberManager;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 成员管理
 * @Author: jeecg-boot
 * @Date:   2020-05-12
 * @Version: V1.0
 */
public interface IMemberManagerService extends IService<MemberManager> {

}
