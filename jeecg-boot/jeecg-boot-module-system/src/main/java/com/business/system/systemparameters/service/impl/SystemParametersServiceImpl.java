package com.business.system.systemparameters.service.impl;

import com.business.system.systemparameters.entity.SystemParameters;
import com.business.system.systemparameters.mapper.SystemParametersMapper;
import com.business.system.systemparameters.service.ISystemParametersService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 系统参数
 * @Author: jeecg-boot
 * @Date:   2020-05-12
 * @Version: V1.0
 */
@Service
public class SystemParametersServiceImpl extends ServiceImpl<SystemParametersMapper, SystemParameters> implements ISystemParametersService {

}
