package com.business.system.takephotos.service.impl;

import com.business.system.takephotos.entity.TakePhotos;
import com.business.system.takephotos.mapper.TakePhotosMapper;
import com.business.system.takephotos.service.ITakePhotosService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 随手拍
 * @Author: jeecg-boot
 * @Date:   2020-05-13
 * @Version: V1.0
 */
@Service
public class TakePhotosServiceImpl extends ServiceImpl<TakePhotosMapper, TakePhotos> implements ITakePhotosService {

}
