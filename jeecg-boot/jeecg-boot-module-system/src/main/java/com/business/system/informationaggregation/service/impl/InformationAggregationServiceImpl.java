package com.business.system.informationaggregation.service.impl;

import com.business.system.informationaggregation.entity.InformationAggregation;
import com.business.system.informationaggregation.mapper.InformationAggregationMapper;
import com.business.system.informationaggregation.service.IInformationAggregationService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 信息汇总
 * @Author: jeecg-boot
 * @Date:   2020-05-13
 * @Version: V1.0
 */
@Service
public class InformationAggregationServiceImpl extends ServiceImpl<InformationAggregationMapper, InformationAggregation> implements IInformationAggregationService {

}
