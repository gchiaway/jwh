package com.business.system.takephotos.service;

import com.business.system.takephotos.entity.TakePhotos;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 随手拍
 * @Author: jeecg-boot
 * @Date:   2020-05-13
 * @Version: V1.0
 */
public interface ITakePhotosService extends IService<TakePhotos> {

}
