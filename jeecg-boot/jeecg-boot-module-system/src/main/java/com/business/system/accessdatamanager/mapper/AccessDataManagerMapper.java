package com.business.system.accessdatamanager.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.business.system.accessdatamanager.entity.AccessDataManager;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 数据管理
 * @Author: jeecg-boot
 * @Date:   2020-05-12
 * @Version: V1.0
 */
public interface AccessDataManagerMapper extends BaseMapper<AccessDataManager> {

}
