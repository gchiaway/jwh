package com.business.system.vote.mapper;

import java.util.List;
import com.business.system.vote.entity.VoteOption;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 投票选项
 * @Author: jeecg-boot
 * @Date:   2020-05-13
 * @Version: V1.0
 */
public interface VoteOptionMapper extends BaseMapper<VoteOption> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<VoteOption> selectByMainId(@Param("mainId") String mainId);
}
