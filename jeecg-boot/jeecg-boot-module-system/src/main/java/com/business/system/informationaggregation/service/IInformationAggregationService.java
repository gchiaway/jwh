package com.business.system.informationaggregation.service;

import com.business.system.informationaggregation.entity.InformationAggregation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 信息汇总
 * @Author: jeecg-boot
 * @Date:   2020-05-13
 * @Version: V1.0
 */
public interface IInformationAggregationService extends IService<InformationAggregation> {

}
