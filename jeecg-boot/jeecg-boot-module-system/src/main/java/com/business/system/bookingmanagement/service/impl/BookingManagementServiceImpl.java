package com.business.system.bookingmanagement.service.impl;

import com.business.system.bookingmanagement.entity.BookingManagement;
import com.business.system.bookingmanagement.mapper.BookingManagementMapper;
import com.business.system.bookingmanagement.service.IBookingManagementService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 预约管理
 * @Author: jeecg-boot
 * @Date:   2020-05-13
 * @Version: V1.0
 */
@Service
public class BookingManagementServiceImpl extends ServiceImpl<BookingManagementMapper, BookingManagement> implements IBookingManagementService {

}
