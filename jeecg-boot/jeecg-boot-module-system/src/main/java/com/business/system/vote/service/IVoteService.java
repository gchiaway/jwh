package com.business.system.vote.service;

import com.business.system.vote.entity.VoteOption;
import com.business.system.vote.entity.Vote;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 投票
 * @Author: jeecg-boot
 * @Date:   2020-05-13
 * @Version: V1.0
 */
public interface IVoteService extends IService<Vote> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(Vote vote,List<VoteOption> voteOptionList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(Vote vote,List<VoteOption> voteOptionList);
	
	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);
	
}
