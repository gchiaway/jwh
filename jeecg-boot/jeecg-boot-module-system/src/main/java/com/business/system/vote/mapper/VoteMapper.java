package com.business.system.vote.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.business.system.vote.entity.Vote;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 投票
 * @Author: jeecg-boot
 * @Date:   2020-05-13
 * @Version: V1.0
 */
public interface VoteMapper extends BaseMapper<Vote> {

}
