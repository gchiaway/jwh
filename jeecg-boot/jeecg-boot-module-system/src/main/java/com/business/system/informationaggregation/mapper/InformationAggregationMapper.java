package com.business.system.informationaggregation.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.business.system.informationaggregation.entity.InformationAggregation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 信息汇总
 * @Author: jeecg-boot
 * @Date:   2020-05-13
 * @Version: V1.0
 */
public interface InformationAggregationMapper extends BaseMapper<InformationAggregation> {

}
