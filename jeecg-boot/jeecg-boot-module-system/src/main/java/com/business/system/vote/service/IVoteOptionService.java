package com.business.system.vote.service;

import com.business.system.vote.entity.VoteOption;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 投票选项
 * @Author: jeecg-boot
 * @Date:   2020-05-13
 * @Version: V1.0
 */
public interface IVoteOptionService extends IService<VoteOption> {

	public List<VoteOption> selectByMainId(String mainId);
}
