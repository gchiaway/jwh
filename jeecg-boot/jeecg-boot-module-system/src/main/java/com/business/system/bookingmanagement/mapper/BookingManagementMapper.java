package com.business.system.bookingmanagement.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.business.system.bookingmanagement.entity.BookingManagement;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 预约管理
 * @Author: jeecg-boot
 * @Date:   2020-05-13
 * @Version: V1.0
 */
public interface BookingManagementMapper extends BaseMapper<BookingManagement> {

}
