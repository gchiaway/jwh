package com.business.system.bookingmanagement.service;

import com.business.system.bookingmanagement.entity.BookingManagement;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 预约管理
 * @Author: jeecg-boot
 * @Date:   2020-05-13
 * @Version: V1.0
 */
public interface IBookingManagementService extends IService<BookingManagement> {

}
